//Sean Murphy
//1935620

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = {
            new Bicycle("Swag Bikes", 16, 32),
            new Bicycle("Cool Bikes", 48, 64),
            new Bicycle("Bad Bikes", 8, 16),
            new Bicycle("B i k e", 420, 9001)};
        for (Bicycle bicycle : bikes) {
            System.out.println(bicycle);
        }
    }
}
