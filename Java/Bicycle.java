//Sean Murphy
//1935620

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String toString(){
        return("Manufacturer: " + this.manufacturer + "\nNumber of Gears: " + this.numberGears + "\nMax Speed: " + this.maxSpeed + "\n");
    }
}